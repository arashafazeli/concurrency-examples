# conc_fut.py

import os, time
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

def my_fn(n, name):
    time.sleep(n)
    print(f"Done with {name}\tPID: {os.getpid()}\tPPID: {os.getppid()}")


if __name__ == "__main__":
    with ThreadPoolExecutor(max_workers=3) as tpe:
        futures = []
        futures.append(tpe.submit(my_fn, 3, "a"))
        futures.append(tpe.submit(my_fn, 3, "b"))
        futures.append(tpe.submit(my_fn, 3, "c"))
        futures.append(tpe.submit(my_fn, 3, "d"))
        futures.append(tpe.submit(my_fn, 3, "e"))
        futures.append(tpe.submit(my_fn, 3, "f"))
        print(futures)
        for f in futures:
            print(f.result())