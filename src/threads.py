# threads.py

import os, time, threading

def my_fn(n, name):
    time.sleep(n)
    print(f"Done with {name}\tPID: {os.getpid()}\tPPID: {os.getppid()}")


if __name__ == "__main__":
    t1 = threading.Thread(target=my_fn, args=(3, "a"))
    t2 = threading.Thread(target=my_fn, args=(3, "b"))
    t1.start()
    t2.start()