# processes.py

import os
import time
import multiprocessing

def my_fn(n):
    time.sleep(n)
    print(f"PID: {os.getpid()}\tPPID: {os.getppid()}")

if __name__ == "__main__":
    # p1 = multiprocessing.Process(target=my_fn, args=(3,))
    # p2 = multiprocessing.Process(target=my_fn, args=(3,))
    # p1.start()
    # p2.start()
    with multiprocessing.Pool() as pool:
        pool.map(my_fn, (3,3))